<?php
use Cake\Core\Configure;

return [
	'WebImobApp.Plugins.ControlPanel.Settings' => [
		'Template' => [
			'theme' => 'DefaultAdminTheme',
			'layout' => 'main'
		]
	]
];
