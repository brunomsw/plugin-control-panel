<?php
/**
 * Routes configuration
 *
 **/

use Cake\Core\Plugin;
use Cake\Routing\Router;

Router::plugin('ControlPanel', ['path' => '/'], function($routes) {
    $routes->connect('/interno', ['controller' => 'PanelPages','action' => 'index']);
});
