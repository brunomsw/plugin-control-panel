<?php

namespace ControlPanel\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Core\Plugin;
use Cake\Core\Configure;

class PanelMenuHelper extends Helper {
	public $helpers = ['Html'];

	protected $_defaultConfig = [];

	public function getPanelMenuItems()
  {
		$plugins = Plugin::loaded();
		$panelMenuItems = [];

    if($this->_detectAppMenuItem())
      $panelMenuItems[] = $this->_View->element('panel_menu');

		foreach($plugins as $plugin){
			if($this->_detectPluginMenuItem($plugin))
				$panelMenuItems[] = $this->_View->element("{$plugin}.panel_menu");
		}
		return $panelMenuItems;
	}

  public function _detectAppMenuItem()
  {
    return file_exists(APP . 'Template/Element/panel_menu.ctp');
  }

	public function _detectPluginMenuItem($plugin)
  {
		if(Configure::read("WebImobApp.Plugins.{$plugin}.Settings.General.display_panel_menu")){
			return file_exists(Plugin::path($plugin) . 'src/Template/Element/panel_menu.ctp');
		}
		return false;
	}

	public function create()
  {
		$html = '';
		foreach($this->getPanelMenuItems() as $item)
			$html .= $item;

		return $html;
	}
}
