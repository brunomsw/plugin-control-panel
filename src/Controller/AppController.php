<?php

namespace ControlPanel\Controller;

use App\Controller\AppController as BaseController;
use Cake\Core\Plugin;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class AppController extends BaseController {
	public $theme = 'DefaultAdminTheme';
	public $layout = 'main';

	public function initialize()
	{
		parent::initialize();

		if(Plugin::loaded('UserAdmin')) {
		    $this->loadComponent('Auth',
		        [
		            'loginAction' => '/interno/login',
		            'loginRedirect' => '/interno',
		            'logoutRedirect' => '/interno/login',
		            'authenticate' => [
		                'Form' => [
		                    'scope' => ['Users.active' => 1],
		                    'userModel' => 'UserAdmin.Users',
		                ]
		            ],
		            'flash' => [
		                'element' => 'DefaultAdminTheme.alert_danger',
		                'key' => 'auth',
		            ],
		        ]
		    );
		    if(!$this->Auth->user())
            	$this->Auth->config('authError', false);
		}
	}

	public function isAuthorized()
    {
        $groupsTable = TableRegistry::get('UserAdmin.Groups');
        $group = $groupsTable->get($this->Auth->user('group_id'));
        if($group->name === Configure::read('WebImobApp.Plugins.UserAdmin.Settings.Authorization.root_group_name')) {
        	$this->set('isRoot', true);
            return true;
        }
        $this->set('isRoot', false);
        return false;
    }
}
