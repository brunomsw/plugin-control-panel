<?php
namespace ControlPanel\Controller;

use ControlPanel\Controller\AppController;

/**
 * PanelPages Controller
 *
 * @property ControlPanel\Model\Table\PanelPagesTable $PanelPages
 */
class PanelPagesController extends AppController {
	public $helpers = ['DefaultAdminTheme.PanelMenu'];
	
	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index() {
	}

    /**
     * Redireciona para a action index()
     * @return \Cake\Network\Response|void
     */
    public function admin()
    {
        return $this->redirect(['action' => 'index']);
    }
}
